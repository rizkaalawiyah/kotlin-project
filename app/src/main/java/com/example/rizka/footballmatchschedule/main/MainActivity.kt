package com.example.rizka.footballmatchschedule.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.ActionBar
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.widget.FrameLayout
import android.widget.ProgressBar
import com.example.rizka.footballmatchschedule.*
import com.example.rizka.footballmatchschedule.api.ApiRepository
import com.example.rizka.footballmatchschedule.model.EventsItem
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var content: FrameLayout? = null
    lateinit var toolbar: ActionBar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        content = findViewById<FrameLayout>(R.id.container)
        val navigation = findViewById<BottomNavigationView>(R.id.navigation)
        toolbar = supportActionBar!!
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        val fragment = PreviousMatch.Companion.newInstance()
        addFrgment(fragment)
        toolbar.title = "Previous Event Match"
    }
    private val mOnNavigationItemSelectedListener = object : BottomNavigationView.OnNavigationItemSelectedListener {
        override fun onNavigationItemSelected(item: MenuItem): Boolean {
            when (item.itemId) {
                R.id.PrevMatch -> {
                    val fragment = PreviousMatch.Companion.newInstance()
                    addFrgment(fragment)
                    toolbar.title = "Previous Event Match"
                    return true
                }
                R.id.NextMatch -> {
                    val fragment = NextMatch.Companion.newInstance()
                    addFrgment(fragment)
                    toolbar.title = "Next Event Match"
                    return true
                }
            }
            return false
        }
    }
    private fun addFrgment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.design_bottom_sheet_slide_in, R.anim.design_bottom_sheet_slide_out)
            .replace(R.id.container, fragment, fragment.javaClass.simpleName)
            .addToBackStack(fragment.javaClass.simpleName)
            .commit()
    }
}

