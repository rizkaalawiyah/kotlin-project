package com.example.rizka.footballmatchschedule


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.example.rizka.footballmatchschedule.api.ApiRepository
import com.example.rizka.footballmatchschedule.main.MainAdapter
import com.example.rizka.footballmatchschedule.main.MainPresenter
import com.example.rizka.footballmatchschedule.main.MainView
import com.example.rizka.footballmatchschedule.model.EventsItem
import com.example.rizka.footballmatchschedule.model.Team
import com.google.gson.Gson

class PreviousMatch : Fragment(), MainView {

    private var eventsPrev: MutableList<EventsItem> = mutableListOf()

    private lateinit var presenter: MainPresenter
    private lateinit var mAdapter: MainAdapter
    private lateinit var progresBar: ProgressBar
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var recyclerViewEvent: RecyclerView


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootview = inflater.inflate(R.layout.fragment_previous_match, container, false)
        progresBar = rootview.findViewById(R.id.pbPrev)
        swipeRefresh = rootview.findViewById(R.id.SwipePrev)
        recyclerViewEvent = rootview.findViewById(R.id.PrevMatch)
        val layoutManager: RecyclerView.LayoutManager = GridLayoutManager(context, 1)
        recyclerViewEvent.layoutManager = layoutManager
        mAdapter = MainAdapter(this.requireActivity(), eventsPrev)
        recyclerViewEvent.adapter = mAdapter
        val apiRepository = ApiRepository()
        val gson = Gson()
        presenter = MainPresenter(this, apiRepository, gson)
        presenter.getPreviousMatch(MainView.idEvents)
        return rootview
    }
    companion object {
            @JvmStatic
            fun newInstance() =
            PreviousMatch().apply {
            arguments = Bundle().apply {
            }
        }
    }
    override fun showLoading() {
        progresBar.visible()
    }
    override fun hideLoading() {
        progresBar.invisible()
    }
    override fun showEventList(data: List<EventsItem>) {
        swipeRefresh.isRefreshing = false
        eventsPrev.clear()
        eventsPrev.addAll(data)
        mAdapter.notifyDataSetChanged()
    }
    override fun showTeamList(data: List<Team>) {

    }
}