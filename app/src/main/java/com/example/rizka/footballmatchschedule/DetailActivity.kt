package com.example.rizka.footballmatchschedule

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.widget.ProgressBar
import com.example.rizka.footballmatchschedule.api.ApiRepository
import com.example.rizka.footballmatchschedule.main.MainView
import com.example.rizka.footballmatchschedule.model.EventsItem
import com.example.rizka.footballmatchschedule.model.Team
import com.google.gson.Gson

class DetailActivity : AppCompatActivity(), MainView {
    private lateinit var progresBar: ProgressBar

    override fun showLoading() {
        progresBar.visible()
    }

    override fun hideLoading() {
        progresBar.invisible()
    }

    override fun showEventList(data: List<EventsItem>) {
    }

    override fun showTeamList(data: List<Team>) {
    }

    var idEvent: String = ""
    var idAway: String = ""
    var idHome: String = ""
    var nameHome: String = ""
    var nameAway: String = ""


    companion object {
        const val idEvents= "id_events"
        const val idTeam2 = "id_Away"
        const val idTeam1 = "id_Home"
        const val NameTeam1 = "home_name"
        const val NameTeam2 = "away_name"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val intent = intent
        idEvent = intent.getStringExtra(idEvents)
        idAway = intent.getStringExtra(idTeam2)
        idHome = intent.getStringExtra(idTeam1)
        nameAway = intent.getStringExtra(NameTeam2)
        nameHome = intent.getStringExtra(NameTeam1)
        val request = ApiRepository()
        val gson = Gson()
        var presenter = DetailPresenter(this, request, gson)
        presenter.getDetailMatch(idEvent)
    }
}
