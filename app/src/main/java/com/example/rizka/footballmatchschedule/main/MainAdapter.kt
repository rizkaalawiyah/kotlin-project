package com.example.rizka.footballmatchschedule.main

import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.rizka.footballmatchschedule.DetailActivity
import com.example.rizka.footballmatchschedule.R
import com.example.rizka.footballmatchschedule.model.EventsItem
import org.jetbrains.anko.startActivity
import java.text.SimpleDateFormat
import java.util.*

class MainAdapter(private val context: Context?, private val eventsItem: List<EventsItem>)
    : RecyclerView.Adapter<MainAdapter.EventHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        EventHolder(
            LayoutInflater.from(context).inflate(
                R.layout.match_list,
                parent,
                false
            )
        )
    override fun getItemCount(): Int = eventsItem.size
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: EventHolder, position: Int) {
        holder.bindItem(eventsItem [position])
        holder.itemView.setOnClickListener {
            context?.startActivity<DetailActivity>(
                DetailActivity.idEvents to eventsItem[position].idEvent,
                DetailActivity.idTeam1 to eventsItem[position].idHomeTeam,
                DetailActivity.NameTeam1 to eventsItem[position].strHomeTeam,
                DetailActivity.idTeam2 to eventsItem[position].idAwayTeam,
                DetailActivity.NameTeam2 to eventsItem[position].strAwayTeam)
        }
    }
    class EventHolder(view:View) :RecyclerView.ViewHolder(view) {
        private val dateEvents = view.findViewById<TextView>(R.id.date)
        private val strHome = view.findViewById<TextView>(R.id.teamName1)
        private val homeScore = view.findViewById<TextView>(R.id.ScoreTeam1)
        private val strAway = view.findViewById<TextView>(R.id.teamName2)
        private val awayScore = view.findViewById<TextView>(R.id.ScoreTeam2)
        @RequiresApi(Build.VERSION_CODES.O)
        fun bindItem (events: EventsItem){
            val formatDate = SimpleDateFormat("yyy-MM-dd", Locale.getDefault())
            val date = formatDate.parse(events.dateEvent)
            val dateText = SimpleDateFormat("EEEE, dd-MM-yyyy", Locale.getDefault())
                .format(date).toString()
            dateEvents.text = dateText
            strAway.text = events.strAwayTeam
            strHome.text = events.strHomeTeam
            homeScore.text = events.intHomeScore
            awayScore.text = events.intAwayScore
        }
    }
}