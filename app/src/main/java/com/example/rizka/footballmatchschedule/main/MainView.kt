package com.example.rizka.footballmatchschedule.main

import com.example.rizka.footballmatchschedule.model.EventsItem
import com.example.rizka.footballmatchschedule.model.Team

interface MainView {
    fun showLoading()
    fun hideLoading()
    fun showEventList(data: List<EventsItem>)
    fun showTeamList(data: List<Team>)

    companion object {
        val idEvents ="4328"
    }
}