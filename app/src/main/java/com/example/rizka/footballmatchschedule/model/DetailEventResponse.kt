package com.example.rizka.footballmatchschedule.model

import com.google.gson.annotations.SerializedName


data class DetailEventResponse(

	@SerializedName("events")
	val events: List<EventsItem>
)